// mama li ni: https://stackoverflow.com/a/10730777/14639652
function textNodesUnder(el){
  var n, a=[], walk=document.createTreeWalker(el,NodeFilter.SHOW_TEXT,null,false);
  while(n=walk.nextNode()) a.push(n);
  return a;
}

function nanpa(n) {
    let ale = (n / 100)|0;
    n = n - ale * 100;
    let mute = (n / 20)|0;
    n = n - mute * 20;
    let luka = (n / 5)|0;
    n = n - luka * 5;
    let tu = (n / 2)|0;
    n = n - tu * 2;
    let wan = n;

    let str = "ale ".repeat(ale) + "mute ".repeat(mute) + "luka ".repeat(luka) + "tu ".repeat(tu) + "wan ".repeat(wan);

    return str.trim();
}

// o sona e ni: ilo sona ni li ike kin!
function ante_e_nanpa_ale() {
    textNodesUnder(document.body).forEach(function(node) {
        node.textContent = node.textContent.replace(/\b([0-9]+)\b/g, function(x) {
            return nanpa(+x);
        });
    });
}
