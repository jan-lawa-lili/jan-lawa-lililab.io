La quatrième planète était celle du businessman. Cet homme était si occupé qu'il ne leva même pas la tête à l'arrivée du petit prince.
-Bonjour, lui dit celui-ci. Votre cigarette est éteinte.
-Trois et deux font cinq. Cinq et sept douze. Douze et trois quinze. Bonjour. Quinze et sept vingt-deux. Vingt-deux et six vingt-huit. Pas de temps de la rallumer. Vingt-six et cinq trente et un. Ouf! Ça fait donc cinq cent un millions six cent vingt-deux mille sept cent trente et un.
-Cinq cents millions de quoi?
-Hein? Tu es toujours là? Cinq cent un million de...je ne sais plus...J'ai tellement de travail! Je suis sérieux, moi, je ne m'amuse pas à des balivernes! Deux et cinq sept...
-Cinq cent millions de quoi, répéta le petit prince qui jamais de sa vie, n'avait-il renoncé à une question, une fois qu'il l'avait posée.
Le businessman leva la tête:
-Depuis cinquante-quatre ans que j'habite cette planète-ci, je n'ai été dérangé que trois fois. La première fois ç'a été, il y a vingt-deux ans, par un hanneton qui était tombé Dieu sait d'où. Il répandait un bruit épouvantable, et j'ai fait quatre erreurs dans une addition. La seconde fois ç'à été, il y a onze ans, par une crise de rhumatisme. Je suis sérieux, moi. La troisième fois...la voici! Je disais donc cinq cent un millions...
-Millions de quoi?
Le businessman comprit qu'il n'était point d'espoir de paix:
-Millions de ces petites choses que l'on voit quelquefois dans le ciel.
-Des mouches?
-Mais non, des petites choses qui brillent.
-Des abeilles?
-Mais non. Des petites choses dorées qui font rêvasser les fainéants. Mais je suis sérieux, moi! Je n'ai pas le temps de rêvasser.
-Ah! des étoiles?
-C'est bien ça. Des étoiles.
-Et que fais-tu des cinq cent millions d'étoiles?
-Cinq cent un millions six cent vingt-deux mille sept cent trente et un. Je suis un homme sérieux, moi, je suis précis.
-Et que fais-tu de ces étoiles?
-Ce que j'en fais?
-Oui.
-Rien. Je les possède.
-Tu possèdes les étoiles?
-Oui.
-Mais j'ai déjà vu un roi qui...
-Les rois ne possèdent pas. Ils "règnent" sur. C'est très différent.
-Et à quoi cela te sert-il de posséder les étoiles?
-Ça me sert à être riche.
-Et à quoi cela te sert-il d'être riche?
-A acheter d'autres étoiles, si quelqu'un en trouve.
Celui-là, se dit en lui-même le petit prince, il raisonne un peu comme mon ivrogne.
Cependant il posa encore des questions:
-Comment peut-on posséder les étoiles?
-A qui sont-elles? riposta, grincheux, le businessman.
-Je ne sais pas. A personne.
-Alors elles sont à moi, car j'y ai pensé le premier.
-Ça suffit?
-Bien sûr. Quand tu trouves un diamant qui n'est à personne, il est à toi. Quand tu trouves une île qui n'est à personne, elle est à toi. Quand tu as une idée le premier, tu la fais breveter: elle est à toi. Et moi je possède les étoiles, puisque jamais personne avant moi n'a songé à les posséder.
-Ça c'est vrai, dit le petit prince. Et qu'en fais-tu?
-Je les gère. Je les compte et je les recompte, dit le businessman. C'est difficile. Mais je suis un homme sérieux!
Le petit prince n'était pas satisfait encore.
-Moi, si je possède un foulard, je puis le mettre autour de mon cou et l'emporter. Moi, si je possède une fleur, je puis cueillir ma fleur et l'emporter. Mais tu ne peux pas cueillir les étoiles!
-Non, mais je puis les placer en banque.
-Qu'est-ce que ça veut dire?
-Ça veut dire que j'écris sur un petit papier le nombre de mes étoiles. Et puis j'enferme à clef ce papier-là dans un tiroir.
-Et c'est tout?
-Ça suffit!
C'est amusant, pensa le petit prince. C'est assez poétique. Mais ce n'est pas très sérieux.
Le petit prince avait sur les choses sérieuses des idées très différentes des idées des grandes personnes.
-Moi, dit-il encore, je possède une fleur que j'arrose tous les jours. Je possède trois volcans que je ramone toutes les semaines. Car je ramone aussi celui qui est éteint. On ne sait jamais. C'est utile à mes volcans, et c'est aussi utile à ma fleur, que je les possède. Mais tu n'est pas utile aux étoiles...
Le businessman ouvrit la bouche mais ne trouva rien à répondre, et le petit prince s'en fut.
Les grandes personnes sont décidément tout à fait extraordinaires, se disait-il en lui même durant son voyage.
