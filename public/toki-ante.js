function show_toki_ante(toki) {
    let path = location.pathname.split('/');
    let num = +path[path.length - 1].replace(/.html$/, '');

    fetch('toki-' + toki + '/' + num + '.txt')
        .then(function(res) {
            return res.text();
        })
        .then(function(text) {

            let sentences = text.split('\n');

            document.querySelectorAll('[data-nanpa-toki]').forEach(function(p) {
                let nanpa = +p.dataset['nanpaToki'] - 1;

                let text = sentences[nanpa];

                let newElement = document.createElement('p');
                newElement.textContent = text;
                newElement.classList.add('toki-ante')
                let nanpaToki = document.createElement('div');
                nanpaToki.textContent = nanpa + 1;
                nanpaToki.classList.add('nanpa-toki')
                newElement.appendChild(nanpaToki)
                p.after(newElement);
            });
        });
}

document.addEventListener('DOMContentLoaded', function() {
    if(window.location.search == '?pali') {
        show_toki_ante('kanse')
    }
});
