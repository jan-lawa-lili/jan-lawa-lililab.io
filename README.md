jan lawa lili - toki ante sin
=============================

sina wile jo e sona pi lipu ni kepeken toki pona la, o lukin e [lipu sinpin](https://jan-lawa-lili.gitlab.io/) :-)

## What is this?

This is a rehost and revision of *jan lawa lili*, the toki pona
translation of *Le Petit Prince* (The Little Prince).

The original toki pona translation was written by Michael Fridman. The
original blog posts are gone ([except on the wayback
machine](https://web.archive.org/web/20161027090542/http://failbluedot.com:80/toki_pona/jan_lawa_lili)).

 

This new translation:

- corrects a few typos, spelling errors, inconsistencies and mistakes
- changes some formulations that felt unsatisfying (for instance, we
  use *soweli len* to mean *sheep*, instead of the original *soweli
  meli*)
- adds missing parts

We felt that the original translation missed some important parts of
the text here and there. For instance, in the second chapter:

> *The first night, then, I went to sleep on the sand, a thousand
> miles from any human habitation.*

was translated as:

> *mi lape* (I slept)

Even though toki pona encourages simpler sentences and focusing on the
essentials, we felt that this was much too simplified to still reflect
the spirit of Saint-Exupéry's original text. In this version, the same
sentence is translated as:

> *tenpo pimeja nanpa wan la, mi lape lon ko kiwen. mi lon weka mute
> pi ma tomo.*

## lipu pi nasin lawa (License)

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>
