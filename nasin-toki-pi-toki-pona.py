#!/usr/bin/env python3

"""
TODO
- (args) Clearer flags for strict pu/ku lili/ku suli, based on lipu Linku
- sentence without a `li/o` but a `e`
"""

import re
import argparse

arg_parser = argparse.ArgumentParser()

arg_parser.add_argument('-n', '--nimi-sin', action='append', help="nimi sin to mark as ok")
arg_parser.add_argument('--literary', action='store_true', help="Enable pendantic punctuation")
arg_parser.add_argument('--ignore-foreign', action='store_true', help="Skip foreign language lines (>3 words, with >1/2 of non-toki pona words)")
arg_parser.add_argument('--allow-foreign-nouns', action='store_true', help="Allow foreign nouns (eg.: jan John)")
arg_parser.add_argument('--warn', action='store_true', help="Warn of things that are not errors per se")
arg_parser.add_argument('--suspicious', action='store_true', help="Check for suspicious usages of grammar (caution: yields false positives)")
arg_parser.add_argument('-a', '--all', action='store_true', help="Enable all checks")
arg_parser.add_argument('files', nargs='*')

args = arg_parser.parse_args()

if args.all:
    args.literary = True
    args.warn = True
    args.suspicious = True


SENTENCE_BEGIN = r'(^|[:;.!?,“”"] ?|\bla[, ]*)'
PARTICLES = 'en|li|e|la|pi|o|anu'
PREPOSITIONS = 'lon|tawa|tan|sama|kepeken'

nimi_pu = 'a|kin|akesi|ala|alasa|ale|ali|anpa|ante|anu|awen|e|en|esun|ijo|ike|ilo|insa|jaki|jan|jelo|jo|kala|kalama|kama|kasi|ken|kepeken|kili|kiwen|ko|kon|kule|kulupu|kute|la|lape|laso|lawa|len|lete|li|lili|linja|lipu|loje|lon|luka|lukin|oko|lupa|ma|mama|mani|meli|mi|mije|moku|moli|monsi|mu|mun|musi|mute|nanpa|nasa|nasin|nena|ni|nimi|noka|o|olin|ona|open|pakala|pali|palisa|pan|pana|pi|pilin|pimeja|pini|pipi|poka|poki|pona|pu|sama|seli|selo|seme|sewi|sijelo|sike|sin|namako|sina|sinpin|sitelen|sona|soweli|suli|suno|supa|suwi|tan|taso|tawa|telo|tenpo|toki|tomo|tonsi|tu|unpa|uta|utala|walo|wan|waso|wawa|weka|wile|n'.split('|')

all_nimi = nimi_pu + (args.nimi_sin or [])
valid_nimi = '(' + '|'.join(all_nimi) + ')'

proper_nouns = "((A|An|E|En|I|In|O|On|U|Un|Ja|Jan|Je|Jen|Jo|Jon|Ju|Jun|Ka|Kan|Ke|Ken|Ki|Kin|Ko|Kon|Ku|Kun|La|Lan|Le|Len|Li|Lin|Lo|Lon|Lu|Lun|Ma|Man|Me|Men|Mi|Min|Mo|Mon|Mu|Mun|Na|Nan|Ne|Nen|Ni|Nin|No|Non|Nu|Nun|Pa|Pan|Pe|Pen|Pi|Pin|Po|Pon|Pu|Pun|Sa|San|Se|Sen|Si|Sin|So|Son|Su|Sun|Ta|Tan|Te|Ten|To|Ton|Tu|Tun|Wa|Wan|We|Wen|Wi|Win)(ja|jan|je|jen|jo|jon|ju|jun|ka|kan|ke|ken|ki|kin|ko|kon|ku|kun|la|lan|le|len|li|lin|lo|lon|lu|lun|ma|man|me|men|mi|min|mo|mon|mu|mun|na|nan|ne|nen|ni|nin|no|non|nu|nun|pa|pan|pe|pen|pi|pin|po|pon|pu|pun|sa|san|se|sen|si|sin|so|son|su|sun|ta|tan|te|ten|to|ton|tu|tun|wa|wan|we|wen|wi|win)*)"

if args.allow_foreign_nouns:
    proper_nouns = '([A-Z][a-z]*)'

spelling_regex = '^(' + '|'.join([valid_nimi, proper_nouns]) + ')$'


def all_indexes_of(word, line):
    indexes = []

    begin = 0

    while True:
        m = re.search(r'\b' + word + r'\b', line)

        if m is None:
            break

        idx = m.start()

        indexes.append(begin + idx)
        begin += idx + 1
        line = line[idx + 1:]

    return indexes


def process_file(fname):
    global last_error_on_line

    file_display_name = '(stdin)' if fname == '/dev/stdin' else fname

    last_error_on_line = -1

    def error_msg(line, num, error_at_char, error_msg):
        global last_error_on_line

        if last_error_on_line != num:
            if last_error_on_line != -1:
                print()
            print(line)

        print('  ', file_display_name, ':', num, ':', error_at_char, ' ', error_msg, sep='')
        last_error_on_line = num

    def test_regex(regex, original_line, num, msg_format, filter=None, debug=False):

        line = original_line

        if filter is None:
            filter = lambda line, match: True

        begin = 0

        while len(line) > 0:
            m = re.search(regex, line)


            if debug:
                print(line, ':', regex, '=>', m)

            msg = msg_format

            if not m:
                # No more matches to test
                break

            if m and filter(line, m):
                error_at_char = begin + m.start()

                for i, g in enumerate(m.groups()):
                    group_num = i + 1
                    if g is not None:
                        msg = msg.replace(f'${group_num}', g)

                error_msg(line, num, error_at_char, msg)

            begin += m.end() + 1
            line = original_line[begin:]


    with open(fname) as f:
        for num, line in enumerate(f):
            num = num + 1
            line = line[:-1] # remove the \n

            # Spelling check
            bad_words = set()
            words = re.sub('[^a-zA-Z ]+', ' ', line).split()

            for word in words:
                m = re.match(spelling_regex, word)

                if not m:
                    bad_words.add(word)

            spelling_errors = []
            for word in bad_words:
                for index in all_indexes_of(word, line):
                    spelling_errors.append((index, word))

            if (args.ignore_foreign and
                len(words) > 3 and
                len(spelling_errors) / len(words) > 0.5):
                # Skip foreign languages
                continue

            for index, word in sorted(spelling_errors, key=lambda x: x[0]):
                error_msg(line, num, index, 'Unknown word: ' + word)

            # Basic grammar checks
            # test_regex(r', *taso', line, num, 'taso can\'t join two sentences')
            test_regex(r'\b(' + PARTICLES + r'|mi|sina)\b +\1\b', line, num, '`$1` appears twice')

            if args.suspicious:
                test_regex(SENTENCE_BEGIN + r'(mi|sina) +li\b', line, num, '`$2` used with `li`')
                test_regex(r'(\bpi +[a-zA-Z]+( +(li|e|pi|en|la|anu|o)\b|[:;.,]|$))', line, num, 'suspicious usage of `pi` here: `$1`')
                test_regex(r'\bli pi\b', line, num, 'are you sure about that `li pi`?')

                test_regex(r'\b(mi|sina|li|o) (lon|sama|tan)( ala)? e\b', line, num, '`$2` as an action verbe (`$2 e X`) is uncommon, double check')

                # tawa e X means "to move X". It's suspicious with
                # large/intangible direct objects that are common use
                # cases for "tawa X"
                # TODO: mi tawa e X
                test_regex(r'\b(o|li) tawa e (tomo|ma|mun|nasin|lupa)\b', line, num, '`tawa` as an action verbe is suspicious here, double check')

                def en_after_la(line, m):
                    # `li ... la ... en` might be correct
                    # TODO: li x pi y en z might be accepted
                    return re.search(r'\bla\b', m.group()) is None
                test_regex(r'(\b(li|o)\b|(' + SENTENCE_BEGIN + r'(mi|sina))) +[^:;.!?,]+ +\ben\b',
                           line, num,
                           '`en` is a subject separator, use multiple `li`/`e`',
                           en_after_la)

                test_regex(r'((\b(' + PARTICLES + '|' + PREPOSITIONS + r')\b)|(' + SENTENCE_BEGIN + r'(mi|sina))) +' + proper_nouns,
                           line, num,
                           "suspicious use of unofficial word without a preceding noun")

            if args.literary:
                test_regex(r'(?!^) {2,}', line, num, 'multiple consecutive spaces')
                test_regex(r' +$', line, num, 'trailing spaces at the end of this line')
                test_regex(r' +[:;.!?,”]', line, num, 'no space before this punctuation')
                test_regex(r'\.\.\.', line, num, 'use … instead of ...')
                test_regex(r'"', line, num, 'use “ ” instead of "')

            if args.warn:
                test_regex(r'(^|[:;.!?,“”"] ?|la[, ]*)sina +o ', line, num, '`sina` can be omitted with `o`')
                test_regex(r'\bpi nanpa ((wan|tu|luka|mute|ale|ali) ?)*\b', line, num, '`pi` can be omitted with `nanpa` as an ordinal marker')

                def ill_formed_question(line, m):
                    no_question_word = (
                        'anu' not in m.group() and
                        'seme' not in m.group() and
                        not re.search(r'(.+) +ala +\1', m.group()))

                    return no_question_word
                test_regex(r'(^|[.!?“”"])([^.!?]+\?)', line, num, 'ill-formed question: $2', ill_formed_question)

                # warn of ambiguity in multiple pi
                def no_particle_between_two_pi(line, m):
                    return re.search(r'\b(li|e|en|la|anu|o)\b', m.group()) is None
                test_regex(r'\bpi [^:;.!?,]+ pi\b', line, num, 'multiple `pi` can be ambiguous', no_particle_between_two_pi)
                # TODO: warn of multiple : in the same sentence
                # eg. "mi wile e ni: mi sona e ni: sina wile ala wile moku?"
                # eg. "mi pilin lape tan ni: mi toki e ni tawa sina: 'o pali e moku kepeken nasin mi!'"

for fname in args.files:
    process_file(fname)

if not len(args.files):
    process_file('/dev/stdin')
