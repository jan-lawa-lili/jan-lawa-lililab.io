#!/bin/bash

cd $(realpath --canonicalize-missing "$0/../")

cd public/
for i in *.html
do
    pandoc $i -t plain --wrap=none -o /dev/stdout | sed -r 's/^- +/- /' >${i%.html}.txt
done

../nasin-toki-pi-toki-pona.py -a *.txt -n n --ignore-foreign
